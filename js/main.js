document.addEventListener('DOMContentLoaded', () => {
    const items = document.querySelectorAll('.sub-fifth-section-info')
    let i = 1;
    setInterval(() => {
        const existingAnimatedElement = document.querySelector(".slide-down-animation");
        if (existingAnimatedElement) {
            existingAnimatedElement.classList.remove("slide-down-animation");
        }
        if(items[i]) {
            items[i].querySelector('p').classList.add("slide-down-animation")
        }
        i++
            if(i == items.length) {
                i = 0;
        }
    }, 5000)
})


$('.img-slider').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 100,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    arrows: false,
    button: false,
});


$('.img-grp').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    arrows: false,
    button: false
});

$('.sub-swiper').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    arrows: false,
    button: false,
    asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
    centerMode: true,
    focusOnSelect: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    speed: 300,
    arrows: false,
    button: false,
    asNavFor: '.sub-swiper'
});